//
//  MainViewController.m
//  PI-App-Template
//
//  Created by pi-data on 11.05.18.
//  Copyright © 2018 PI-Data. All rights reserved.
//

#import "MainViewController.h"
#import "IOSDialog.h"

@interface MainViewController ()

@end

@class ComDaimlerPrototypeCommonUIPrototypeDialogDelegate;

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional loadNib after loading the view.
  
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    
    // Adding the swipe gesture on image view
    [self.view addGestureRecognizer:swipeLeft]; 
    [self.view addGestureRecognizer:swipeRight];
    [self.view addGestureRecognizer:swipeUp];
    [self.view addGestureRecognizer:swipeDown];
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
    }
  
    if (swipe.direction == UISwipeGestureRecognizerDirectionUp) {
    }
  
    if (swipe.direction == UISwipeGestureRecognizerDirectionDown) {
    }
    
}

+(void)setDlgDelegate:(ComDaimlerPrototypeCommonUIPrototypeDialogDelegate *)dlgDelegate{
    self.dlgDelegate = dlgDelegate;
}

-(void)setTitle:(NSString *)title {
  [super setTitle:title];
}

@end
