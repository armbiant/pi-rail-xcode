#import "TabFragmentViewController.h"
#import "IOSDialog.h"

@interface TabFragmentViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerLocoList;
@property (weak, nonatomic) IBOutlet UIView *containerControl;
@property (weak, nonatomic) IBOutlet UIView *containerTurnout;
@property (weak, nonatomic) IBOutlet UIView *containerLight;
@property (weak, nonatomic) IBOutlet UIView *containerRails;




@end

@class ComDaimlerPrototypeCommonUIPrototypeDialogDelegate;

@implementation TabFragmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

+(void)setDlgDelegate:(ComDaimlerPrototypeCommonUIPrototypeDialogDelegate *)dlgDelegate{
    self.dlgDelegate = dlgDelegate;
}

- (IBAction)segmentSelected:(UISegmentedControl *)sender {
    [self showSelectedTab: sender.selectedSegmentIndex];
}

- (void)showSelectedTab:(NSInteger)tabIndex {
    NSLog(@"Tab selected: %ld", (long)tabIndex);
    
    if (tabIndex == 0) {
        self.containerLocoList.hidden = false;
    }
    else {
        self.containerLocoList.hidden = true;
    }
    
    if (tabIndex == 1) {
        self.containerControl.hidden = false;
    }
    else {
        self.containerControl.hidden = true;
    }
    
    if (tabIndex == 2) {
        self.containerTurnout.hidden = false;
    }
    else {
        self.containerTurnout.hidden = true;
    }
    
    if (tabIndex == 3) {
        self.containerLight.hidden = false;
    }
    else {
        self.containerLight.hidden = true;
    }
    
    if (tabIndex == 4) {
        self.containerRails.hidden = false;
    }
    else {
        self.containerRails.hidden = true;
    }
}

@end
