//
//  AppDelegate.h
//  PI-App-Template
//
//  Created by pi-data on 11.05.18.
//  Copyright © 2018 PI-Data. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

