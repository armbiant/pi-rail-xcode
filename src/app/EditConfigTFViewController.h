#import <UIKit/UIKit.h>
#import "DialogControllerDelegate.h"
#import "UIViewControllerPI.h"

@interface EditConfigTFViewController : PIIC_UIViewControllerPI

- (void)showSelectedTab:(NSInteger)tabIndex;

@end
