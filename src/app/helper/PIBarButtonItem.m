//
//  PINavigatioItem.m
//  stopstart
//
//  Created by cga on 09.03.19.
//  Copyright © 2019 PI-Data AG. All rights reserved.
//

#import "PIBarButtonItem.h"
#import <JRE.h>
#import "MainViewController.h"
#import "DialogController.h"
#import "IOSDialog.h"
#import "Logger.h"

@implementation PIBarButtonItem

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    self.action = @selector(buttonPressed:);
  }
  return self;
}



- (void)buttonPressed:(PIBarButtonItem *)button {
  PICL_Logger_infoWithNSString_( @"IOSBackButton Pressed" );
  PIIC_UIViewControllerPI *controller = [self getViewController];
  [nil_chk([nil_chk([controller getIosDialog]) getController]) closeWithBoolean:true];
}

- (PIIC_UIViewControllerPI *)getViewController {

  UIView *theView = self.customView;
  if (!theView.superview && [self respondsToSelector:@selector(view)]) {
    theView = [self performSelector:@selector(view)];
  }

  UIView *parent = theView.superview;
  while (parent != nil) {
    UIResponder *nextResponder = [parent nextResponder];
    if ([nextResponder isKindOfClass:[PIIC_UIViewControllerPI class]]) {
      return (PIIC_UIViewControllerPI *) nextResponder;
    }
    else {
      parent = parent.superview;
    }
  }
  return nil;
}


@end
