//
//  InsetLabel.m
//  stopstart
//
//  Created by cga on 06.03.19.
//  Copyright © 2019 PI-Data AG. All rights reserved.
//

#import "InsetLabel.h"

@implementation InsetLabel

@synthesize topInset, leftInset, bottomInset, rightInset;

- (void)drawTextInRect:(CGRect)rect
{
    leftInset = 10;
    UIEdgeInsets insets = {self.topInset, self.leftInset,
        self.bottomInset, self.rightInset};
    
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
