//
//  MainViewController.h
//  PI-App-Template
//
//  Created by pi-data on 11.05.18.
//  Copyright © 2018 PI-Data. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DialogControllerDelegate.h"
#import "UIViewControllerPI.h"

@interface MainViewController : PIIC_UIViewControllerPI

-(void)setTitle:(NSString *)title;

@end

