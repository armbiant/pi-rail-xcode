# PI-Rail-XCode

PI-Rail XCode project for compiling cross generated Objective-C sources, creating native UI and deploying PI-Rail iOS App.

See [PI-Rail-FX](https://gitlab.com/pi-rail/pi-rail-fx) for documentation on desktop app and [PI-Rail-Arduino](https://gitlab.com/pi-rail/pi-rail-arduino) for documentation on firmware.